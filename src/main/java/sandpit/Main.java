package sandpit;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;

import java.util.ArrayList;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.client.rx.rxjava.RxObservable;

public class Main {

	public static void main(String args[]) {

		final WebTarget githubTarget = RxObservable.newClient().target("https://api.github.com/users/");

		ArrayList<String> names = new ArrayList<>();
		names.add("mojombo");
		names.add("defunkt");
		names.add("pjhyett");
		names.add("wycats");
		names.add("sdfj;sksd;f");

		Observable.from(names).flatMap(new Func1<String, Observable<Response>>() {
			@Override
			public Observable<Response> call(String name) {
				return RxObservable.from(githubTarget).path(name).request().rx().get();
			}
		}).subscribe(new Action1<Response>() {

			@Override
			public void call(Response response) {
				System.out.println(response.getStatus());
				System.out.println(response.readEntity(String.class));
			}
		}, new Action1<Throwable>() {

			@Override
			public void call(Throwable throwable) {
				System.err.println("ERROR: " + throwable.getMessage());
			}
		}, new Action0() {
			@Override
			public void call() {
				System.out.println("DONE");
			}
		});

		// Observable.just()
	}
}
